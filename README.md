# HelloWorld

#### 说明

关于项目需求定义, 请参考 https://gitee.com/how-to-do-it/hello-world/issues

#### 运行应用
安装SBT
下载[SBT 1.1.6](https://piccolo.link/sbt-1.1.6.zip)并解压，将`%SBT_HOME%/bin`添加至环境变量PATH。

自动化测试

```
cd HelloWorld
sbt test
```

开发模式运行

```
cd HelloWorld
sbt run
```

生产模式运行

```
cd HelloWorld
sbt dist
```
将生成的分发包`HelloWorld/target/universal/helloworld-1.0.0.zip`上传至服务器并解压，
```
./helloworld-1.0.0/bin/helloworld
```

测试地址
```
http://IP:9000/
```
