package controllers

import javax.inject._
import play.api.Configuration
import play.api.mvc._
import play.core.PlayVersion

@Singleton
class HomeController @Inject()(cc: ControllerComponents, config: Configuration) extends AbstractController(cc) {
  val version = config.get[String]("version")

  def index(who: String) = Action {
    Ok(views.html.index(who, version, PlayVersion.current))
  }

}
