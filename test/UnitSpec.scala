import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import controllers.HomeController
import org.scalatestplus.play._
import play.api.Configuration
import play.api.mvc.Results
import play.api.test.Helpers._
import play.api.test.FakeRequest
/**
 * Unit tests can run without a full Play application.
 */
class UnitSpec extends PlaySpec with Results {

  "HomeController" should {

    "return a valid result with action" in {
      val config = Configuration(ConfigFactory.load("application"))
      val controller = new HomeController(stubControllerComponents(), config)
      val result = controller.index("world")(FakeRequest())
      status(result) mustEqual OK
      contentAsString(result) must include ("Hello World")
    }
  }
}
